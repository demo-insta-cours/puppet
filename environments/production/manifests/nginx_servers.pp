define nginxProxy($listen_port, $proxy, $nginxpath = '/etc/nginx/conf.d/')
{
  file { $name:
     path    => "${nginxpath}${name}.conf",
     content => template("/etc/puppetlabs/code/environments/production/manifests/templates/vhost_proxy.erb")
  }
}

class profile::nginx{

  include 'firewall'

  #if defined(nginx::proxies){
    $files = lookup(nginx::proxies,{})
    create_resources('nginxProxy',$files)
  #}

  #if defined(nginx::firewall::rules){
     $rules = lookup(nginx::firewall::rules,{})
     create_resources('firewall',$rules)
  #}
  
}
