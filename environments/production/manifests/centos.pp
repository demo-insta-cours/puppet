class profile::centos {

  include 'yum'
  
  if requested::packages::os {$packagesOS   = lookup(requested::packages::os)}
  if requested::packages::node {$packagesNode = lookup(requested::packages::node)}

  if $packagesOS{
    create_resources('package',$packagesOS)
  }

  if $packagesNode{
    create_resources('package',$packagesNode)
  }

}
